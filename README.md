# GEN Dashboard
This a `NodeJS app` for displaying `JSON alerts` published at a given `Kafka` topic. This project has a backend, coded in `NodeJS` and a frontend, coded using `HTML` (obviously) and `AngularJS`. The idea is to consume the alerts from the `Kafka` topic at the backend, and using `socket.io`, send them to the frontend displaying them in the browser as a visual dashboard.

Note that, the frontend, has been customised for the `Smart Water` domain, therefore if you want to use it in another domain, you should change the [index.html](https://gitlab.com/ucase/public/gen-dashboard/-/blob/master/public/index.html) in order to show the properties of the specific alerts used in your case.

## Prerequisites
- You need a Kafka Server running, download it at [[1]](https://www.apache.org/dyn/closer.cgi?path=/kafka/2.8.0/kafka_2.13-2.8.0.tgz).
- You need NodeJS, download it at [[2]](https://nodejs.org/es/download/).

## How to use it
1. Clone the repo
2. Run the command `npm install` to install the dependencies
3. Start Kafka and create the required topic (`streams-output`). Use the `commands.txt` file, available at [[3]](https://gitlab.com/ucase/public/sp-architecture/-/blob/master/assets/commands.txt) in order to start `Zookeeper`, `Kafka` and create the topics:
    - First, start, in a new terminal, `Zookeeper` with the following
      command: `bin\windows\zookeeper-server-start.bat config\zookeeper.properties`
    - Second, start, in a new terminal, `Kafka` with the following
      command: `bin\windows\kafka-server-start.bat config\server.properties`
    - Third, create the 5 required topics, in a new terminal, with the following template
      command: `bin\windows\kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --config cleanup.policy=delete --topic streams-input`
    - Note that, all these terminals must be opened in the root folder of the Kafka installation. You can find more info
      about these commands at [[4]](https://kafka.apache.org/quickstart)
4. Once `Zookeeper`, `Kafka` and the topics are created, running and ready, go back to the project's root and run the command `node index.js` to start the application.
5. Go to [http://localhost:3000](http://localhost:3000) to see the dashboard.

## Customisation
- If you want to consume the `JSON alerts` from another topic, you should change it in line 31 at [index.js](https://gitlab.com/ucase/public/gen-dashboard/-/blob/master/index.js)
- If you want your `NodeJS app` to run in a different port than `3000`, you should change it in line 47 at [index.js](https://gitlab.com/ucase/public/gen-dashboard/-/blob/master/index.js)