angular.module("AlertListApp").controller("AlertCtrl", function($scope, $http, $location, $q) {
    $scope.items = [];
    var socket = io();
    socket.on('connect', function() {
        console.log("Connected to socket: " + socket.id);
    });
    socket.on('newAlert', function(data) {
        updateAlertList();
    });

    function refresh() {
        console.log("Refreshing");
        $scope.actionTitle = "Add item";
        $scope.action = "Add";
        $scope.buttonClass = "btn btn-primary";
        $scope.items = [];
        updateAlertList();
    }

    function updateAlertList() {
        $http.get("/alerts").then(function(response) {
            $scope.items = response.data;
        });
    }

    $scope.refresh = function() {
        refresh();
    };
    refresh();
});