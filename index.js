var express = require('express');
var app = express();
const http = require('http');
var path = require('path');
var public = __dirname + "/public/";
var alerts = [];

app.get('/', function(req, res) {
    res.sendFile(path.join(public + "index.html"));
});

app.use('/', express.static(public));
app.get('/alerts', function(req, res, next) {
    console.log("Seding alerts");
    res.json(alerts);
});

const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server, { allowEIO3: true });


io.on('connection', (socket) => {
    console.log("User connected");
});

var kafka = require('kafka-node'),
    Consumer = kafka.Consumer,
    client = new kafka.KafkaClient({kafkaHost: 'localhost:9092'}),
    consumer = new Consumer(client, [{
        topic: 'streams-output',
        partition: 0
    }], {
        autoCommit: false
    });

consumer.on('message', function(message) {
    console.log("*** New alert received: ", message);
    try {
        alerts.push(JSON.parse(message.value));
    } catch (e) {
        console.log("Error parsing JSON");
    }
    io.emit('newAlert', 'ok');
});

server.listen(3000, function() {
    console.log("Server with GUI up and running!");
});